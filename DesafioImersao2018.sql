CREATE TABLE Cliente (
	CPF VARCHAR(12) NOT NULL,
	nome VARCHAR(50) NOT NULL,
	endereco VARCHAR(50),
	sexo CHAR(1),
    dataNascimento TIMESTAMP,
    Telefone VARCHAR(9),

	PRIMARY KEY(CPF),   
	CHECK (sexo in ('M', 'F'))
);

INSERT INTO Cliente VALUES ('11111122222', 'Maria Fernanda Nobrega' , 'R. Dom João V, 120 – Miramar, 21, João Pessoa', 'F', '12/04/1990');
INSERT INTO Cliente VALUES ('96452997623', 'Natalia Campos',  'R. Padre Ibiapina, 56 – Cabo Branco', 'F', '02/07/1988');
INSERT INTO Cliente VALUES ('75432334512', 'Jorge Alencar', 'R. Boa Esperança, 89 – Miramar', 'M', '1972-08-09');
INSERT INTO Cliente VALUES ('9746356654', 'Gabriel Nascimento', 'R. Dom João V, 310 – Miramar', 'M', '12/04/1988');
INSERT INTO Cliente VALUES ('74635928374', 'Leticia Alencar', 'R. da Paz, 21 – Bancários', 'F', '01/09/1988');


CREATE TABLE Carro(
	codigo INT ,
	Modelo VARCHAR(40),
	Marca VARCHAR(40),
    Placa VARCHAR(20),

	PRIMARY KEY (codigo)
	
);

INSERT INTO CARRO VALUES (1, 'SPIN','CHEVROLET','ABC-1342');
INSERT INTO CARRO VALUES (2, 'SIENA', 'FIAT','PPG-7763');
INSERT INTO CARRO VALUES (3, 'SOUL', 'KIA','AAA-5522');
INSERT INTO CARRO VALUES (4, 'SANDERO', 'RENAULT','NAU-8332');


CREATE TABLE Aluga(
    CPF VARCHAR(12) NOT NULL,
    codigo INT ,
    DataDeLocação TIMESTAMP,
    DataDeDevolução TIMESTAMP,
    PRIMARY KEY (CPF,codigo),
    FOREIGN KEY(CPF) REFERENCES Cliente(CPF),
    FOREIGN KEY(codigo) REFERENCES Carro
    
   
) ;


CREATE TABLE Funcionario (
	Codigo VARCHAR(12) NOT NULL,
	Matricula VARCHAR(6) NOT NULL,
	Nome VARCHAR(50)
    );
    
INSERT INTO FUNCIONARIO VALUES (1, '314264','Paulo Andrade');
INSERT INTO FUNCIONARIO VALUES (2, '089785', 'Vitoria Veloso');
	 
    
    
